package dao;

import Model.Section;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.logging.Logger;


public class SectionDAO implements IDAO<Section> {
    private static Logger logger = Logger.getLogger( SectionDAO.class.getName() );

    @Override
    public List<Section> findAll() {
        logger.info( "Récupération" );
        EntityManager em = PersistenceManager.getEntityManager();
        Query query = em.createNamedQuery( "findAllSection" );

        List<Section> list = query.getResultList();
        em.close();
        return list;
    }

    @Override
    public Section find(Object id) throws EntityNotFoundException, EntityNotFoundException {
        EntityManager em = PersistenceManager.getEntityManager();
        Section section = em.find( Section.class, id );
        em.close();
        if ( section == null ) throw new EntityNotFoundException( "Aucune section trouvée !" );
        return section;
    }

    @Override
    public void save(Section entity) {
        EntityManager em = PersistenceManager.getEntityManager();
        try {
            em.getTransaction().begin();
            em.merge( entity );
            em.getTransaction().commit();
        } finally {
            if ( em.getTransaction().isActive() ) {
                em.getTransaction().rollback();
            }
            em.close();
        }
    }

    @Override
    public void delete(Section entity) {

    }

}
