package dao;

import Model.Stagiaire;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.logging.Logger;


public class StagiaireDAO implements IDAO<Stagiaire> {

    private static Logger logger = Logger.getLogger( StagiaireDAO.class.getName() );

    @Override
    public List<Stagiaire> findAll() {
        logger.info( "Récupération" );
        EntityManager em = PersistenceManager.getEntityManager();
        Query query = em.createNamedQuery( "findAllUsers" );

        List<Stagiaire> list = query.getResultList();
        em.close();
        return list;

    }

    @Override
    public Stagiaire find( Object id ) throws EntityNotFoundException {
        EntityManager em = PersistenceManager.getEntityManager();
        Stagiaire stagiaire = em.find( Stagiaire.class, id );
        em.close();
        if ( stagiaire == null ) throw new EntityNotFoundException( "Aucun stagiaire trouvé !" );
        return stagiaire;
    }



    @Override
    public void delete(Stagiaire entity) {

    }

    @Override
    public void save( Stagiaire entity ) {

        EntityManager em = PersistenceManager.getEntityManager();
        try {
            em.getTransaction().begin();
            em.merge( entity );
            em.getTransaction().commit();
        } finally {
            if ( em.getTransaction().isActive() ) {
                em.getTransaction().rollback();
            }
            em.close();
        }
    }

    public Stagiaire auth(String login, String pwd)
    {
        EntityManager em = PersistenceManager.getEntityManager();
        Query query = em.createNamedQuery("login");
        query.setParameter("login", login);
        query.setParameter("password", pwd);

        Stagiaire stagiaire = (Stagiaire) query.getSingleResult();
        em.close();
        return stagiaire;

    }
}
