package dao;

import Model.QcmSection;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.logging.Logger;

public class QcmSectionDAO implements IDAO<QcmSection> {

    private static Logger logger = Logger.getLogger( QcmSectionDAO.class.getName() );

    @Override
    public List<QcmSection> findAll() {
        logger.info( "Récupération" );
        EntityManager em = PersistenceManager.getEntityManager();
        Query query = em.createNamedQuery( "findAllQcmSection" );

        List<QcmSection> list = query.getResultList();
        em.close();
        return list;
    }

    @Override
    public QcmSection find( Object id ) throws EntityNotFoundException {
        EntityManager em = PersistenceManager.getEntityManager();
        QcmSection qs = em.find( QcmSection.class, id );
        em.close();
        if ( qs == null ) throw new EntityNotFoundException( "Aucune relation QCM-Section trouvé !" );
        return qs;
    }

    @Override
    public void delete(QcmSection entity) {

    }

    @Override
    public void save(QcmSection entity ) {

        EntityManager em = PersistenceManager.getEntityManager();
        try {
            em.getTransaction().begin();
            em.merge( entity );
            em.getTransaction().commit();
        } finally {
            if ( em.getTransaction().isActive() ) {
                em.getTransaction().rollback();
            }
            em.close();
        }
    }

    public QcmSection findQcmSectionBySection(int id)
    {
        EntityManager em = PersistenceManager.getEntityManager();
        Query query = em.createNamedQuery("findQcmSectionBySection");
        query.setParameter("id", id);

        QcmSection qs = (QcmSection) query.getSingleResult();
        em.close();
        return qs;
    }

    public QcmSection findQcmSectionByQcm(int id)
    {
        EntityManager em = PersistenceManager.getEntityManager();
        Query query = em.createNamedQuery("findQcmSectionByQcm");
        query.setParameter("id", id);

        QcmSection qs = (QcmSection) query.getSingleResult();
        em.close();
        return qs;
    }
}
