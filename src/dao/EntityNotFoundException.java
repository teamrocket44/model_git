package dao;

/**
 * Created by ssylla on 11/01/2018.
 */
public class EntityNotFoundException extends Exception {

    public EntityNotFoundException(String message ) {

        super( message );
    }
}
