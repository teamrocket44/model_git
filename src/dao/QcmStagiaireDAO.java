package dao;

import Model.QcmStagiaire;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.logging.Logger;

public class QcmStagiaireDAO implements IDAO<QcmStagiaire> {

    private static Logger logger = Logger.getLogger( QcmSectionDAO.class.getName() );

    @Override
    public List<QcmStagiaire> findAll() {
        logger.info( "Récupération" );
        EntityManager em = PersistenceManager.getEntityManager();
        Query query = em.createNamedQuery( "findAllQcmStagiaire" );

        List<QcmStagiaire> list = query.getResultList();
        em.close();
        return list;
    }

    @Override
    public QcmStagiaire find( Object id ) throws EntityNotFoundException {
        EntityManager em = PersistenceManager.getEntityManager();
        QcmStagiaire qs = em.find( QcmStagiaire.class, id );
        em.close();
        if ( qs == null ) throw new EntityNotFoundException( "Aucune relation Qcm-Stagiaire trouvé !" );
        return qs;
    }



    @Override
    public void delete(QcmStagiaire entity) {

    }

    @Override
    public void save(QcmStagiaire entity ) {

        EntityManager em = PersistenceManager.getEntityManager();
        try {
            em.getTransaction().begin();
            em.merge( entity );
            em.getTransaction().commit();
        } finally {
            if ( em.getTransaction().isActive() ) {
                em.getTransaction().rollback();
            }
            em.close();
        }
    }

    public QcmStagiaire findQcmStagiaireByStagiaire(int id)
    {
        EntityManager em = PersistenceManager.getEntityManager();
        Query query = em.createNamedQuery("findQcmStagiaireByStagiaire");
        query.setParameter("id", id);

        QcmStagiaire qs = (QcmStagiaire) query.getSingleResult();
        em.close();
        return qs;
    }

    public QcmStagiaire findQcmStagiaireByQcm(int id)
    {
        EntityManager em = PersistenceManager.getEntityManager();
        Query query = em.createNamedQuery("findQcmStagiaireByQcm");
        query.setParameter("id", id);

        QcmStagiaire qs = (QcmStagiaire) query.getSingleResult();
        em.close();
        return qs;
    }
}
