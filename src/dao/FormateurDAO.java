package dao;

import Model.Formateur;
import Model.Stagiaire;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.logging.Logger;

public class FormateurDAO implements IDAO<Formateur> {

    private static Logger logger = Logger.getLogger( StagiaireDAO.class.getName() );

    @Override
    public List<Formateur> findAll() {
        logger.info( "Récupération" );
        EntityManager em = PersistenceManager.getEntityManager();
        Query query = em.createNamedQuery( "findAllFormateur" );

        List<Formateur> list = query.getResultList();
        em.close();
        return list;
    }

    @Override
    public Formateur find(Object id) throws EntityNotFoundException, EntityNotFoundException {
        EntityManager em = PersistenceManager.getEntityManager();
        Formateur formateur = em.find(Formateur.class, id);
        em.close();
        if (formateur == null) throw new EntityNotFoundException("Aucune réponse trouvée !");
        return formateur;
    }

    @Override
    public void save(Formateur entity) {
        EntityManager em = PersistenceManager.getEntityManager();
        try {
            em.getTransaction().begin();
            em.merge(entity);
            em.getTransaction().commit();
        } finally {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            em.close();
        }
    }

    @Override
    public void delete(Formateur entity) {

    }

    public Formateur auth(String login, String pwd)
    {
        EntityManager em = PersistenceManager.getEntityManager();
        Query query = em.createNamedQuery("loginF");
        query.setParameter("loginF", login);
        query.setParameter("passwordF", pwd);

        Formateur formateur = (Formateur) query.getSingleResult();
        em.close();
        return formateur;

    }
}
