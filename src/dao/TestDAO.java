package dao;

import Model.Test;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.logging.Logger;

public class TestDAO implements IDAO<Test> {

    private static Logger logger = Logger.getLogger( StagiaireDAO.class.getName() );

    @Override
    public List<Test> findAll() {
        logger.info("Récupération");
        EntityManager em = PersistenceManager.getEntityManager();
        Query query = em.createNamedQuery("findAllTest");

        List<Test> list = query.getResultList();
        em.close();
        return list;
    }

    @Override
    public Test find(Object id) throws EntityNotFoundException, EntityNotFoundException {
        EntityManager em = PersistenceManager.getEntityManager();
        Test test = em.find(Test.class, id);
        em.close();
        if (test == null) throw new EntityNotFoundException("Aucun test trouvé");
        return test;
    }

    @Override
    public void save(Test entity) {

        EntityManager em = PersistenceManager.getEntityManager();
        try {
            em.getTransaction().begin();
            em.merge( entity );
            em.getTransaction().commit();
        } finally {
            if ( em.getTransaction().isActive() ) {
                em.getTransaction().rollback();
            }
            em.close();
        }
    }

    @Override
    public void delete(Test entity) {

    }
}
