package dao;

import Model.Reponse;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.logging.Logger;

public class ReponseDAO implements IDAO<Reponse> {
    private static Logger logger = Logger.getLogger(ReponseDAO.class.getName());

    @Override
    public List<Reponse> findAll() {
        logger.info("Récupération");
        EntityManager em = PersistenceManager.getEntityManager();
        Query query = em.createNamedQuery("findAllReponse");

        List<Reponse> list = query.getResultList();
        em.close();
        return list;
    }

    @Override
    public Reponse find(Object id) throws EntityNotFoundException, EntityNotFoundException {
        EntityManager em = PersistenceManager.getEntityManager();
        Reponse reponse = em.find(Reponse.class, id);
        em.close();
        if (reponse == null) throw new EntityNotFoundException("Aucune réponse trouvée !");
        return reponse;
    }

    @Override
    public void save(Reponse entity) {
        EntityManager em = PersistenceManager.getEntityManager();
        try {
            em.getTransaction().begin();
            em.merge(entity);
            em.getTransaction().commit();
        } finally {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            em.close();
        }
    }

    @Override
    public void delete(Reponse entity) {

    }
}
