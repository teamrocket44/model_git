package dao;

import javax.persistence.*;

/**
 * Created by ssylla on 11/01/2018.
 */
public class PersistenceManager {

    private static final String PERSISTENCE_UNIT = "NewPersistenceUnit";
    private static EntityManagerFactory emf;

    public static EntityManagerFactory getEntityManagerFactory() {
        if ( null == emf ) {
            emf = Persistence.createEntityManagerFactory( PERSISTENCE_UNIT );
        }
        return emf;
    }

    public static void closeEntityManagerFactory() {
        if ( null != emf && emf.isOpen() ) {
            emf.close();
        }
    }
    public static EntityManager getEntityManager() {
        return getEntityManagerFactory().createEntityManager();
    }
}
