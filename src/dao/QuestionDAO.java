package dao;

import Model.QCM;
import Model.Question;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.logging.Logger;

public class QuestionDAO implements IDAO<Question>{
    private static Logger logger = Logger.getLogger( QcmDAO.class.getName() );


    @Override
    public List<Question> findAll() {
        logger.info( "Récupération" );
        EntityManager em = PersistenceManager.getEntityManager();
        Query query = em.createNamedQuery( "findAllQuestion" );

        List<Question> list = query.getResultList();
        em.close();
        return list;
    }

    @Override
    public Question find(Object id) throws EntityNotFoundException, EntityNotFoundException {
        EntityManager em = PersistenceManager.getEntityManager();
        Question question = em.find( Question.class, id );
        em.close();
        if ( question == null ) throw new EntityNotFoundException( "Aucune question trouvée !" );
        return question;
    }

    @Override
    public void save(Question entity) {
        EntityManager em = PersistenceManager.getEntityManager();
        try {
            em.getTransaction().begin();
            em.merge( entity );
            em.getTransaction().commit();
        } finally {
            if ( em.getTransaction().isActive() ) {
                em.getTransaction().rollback();
            }
            em.close();
        }
    }

    @Override
    public void delete(Question entity) {

    }
}