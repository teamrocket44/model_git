package dao;

import java.util.List;

/**
 * Created by ssylla on 11/01/2018.
 */
public interface IDAO<T> {

    public List<T> findAll();

    public T find(Object id) throws EntityNotFoundException, EntityNotFoundException;

    public void save(T entity);

    public void delete(T entity);
}
