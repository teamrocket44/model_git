package dao;

import Model.QCM;
import Model.QcmSection;
import Model.Stagiaire;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.logging.Logger;

public class QcmDAO implements IDAO<QCM> {

    private static Logger logger = Logger.getLogger( QcmDAO.class.getName() );

    @Override
    public List<QCM> findAll() {

        logger.info( "Récupération" );
        EntityManager em = PersistenceManager.getEntityManager();
        Query query = em.createNamedQuery( "findAllQcm" );

        List<QCM> list = query.getResultList();
        em.close();
        return list;

    }

    @Override
    public QCM find(Object id) throws EntityNotFoundException, EntityNotFoundException {
        EntityManager em = PersistenceManager.getEntityManager();
        QCM qcm = em.find( QCM.class, id );
        em.close();
        if ( qcm == null ) throw new EntityNotFoundException( "Aucun qcm trouvé !" );
        return qcm;
    }

    @Override
    public void save(QCM entity) {

        EntityManager em = PersistenceManager.getEntityManager();
        try {
            em.getTransaction().begin();
            em.merge( entity );
            em.getTransaction().commit();
        } finally {
            if ( em.getTransaction().isActive() ) {
                em.getTransaction().rollback();
            }
            em.close();
        }
    }

    @Override
    public void delete(QCM entity) {

    }
}
