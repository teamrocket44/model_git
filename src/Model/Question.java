package Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

@Entity
@Table(name = "question")
@NamedQueries( {
        @NamedQuery( name= "findAllQuestion", query = "SELECT question from Question as question"),
        @NamedQuery( name= "FindQuestionById", query= "SELECT question from Question as question where question.id = :id")
})
public class Question  implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int idQuestion;
    @Column(length=500)
    private String libelleQuestion;
    private String imageQuestion;
    private String typeQuestion;
    private Boolean marqueQuestion;

    @ManyToOne
    @JoinColumn(name = "fkSection")
    private Section section;

    @OneToMany(mappedBy = "question", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Reponse> listReponse;

    @OneToMany(mappedBy = "question")
    private Set<TestQuestion> listTest_question;


    public Question(String libelleQuestion, String imageQuestion, String typeQuestion, Boolean marqueQuestion)
    {
        this.listReponse = new HashSet<>();
        this.listTest_question = new HashSet<>();
        this.libelleQuestion=libelleQuestion;
        this.imageQuestion=imageQuestion;
        this.typeQuestion=typeQuestion;
        this.marqueQuestion=marqueQuestion;
    }
    public Question()
    {
        this.listReponse = new HashSet<>();
        this.listTest_question = new HashSet<>();
    }
    public int getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getlibelleQuestion() {
        return libelleQuestion;
    }

    public void setlibelleQuestion(String libelleQuestion) {
        this.libelleQuestion = libelleQuestion;
    }

    public String getImageQuestion() {
        return imageQuestion;
    }

    public void setImageQuestion(String imageQuestion) {
        this.imageQuestion = imageQuestion;
    }

    public String getTypeQuestion() {
        return typeQuestion;
    }

    public void setTypeQuestion(String typeQuestion) {
        this.typeQuestion = typeQuestion;
    }

    public Boolean getMarqueQuestion() {
        return marqueQuestion;
    }

    public void setMarqueQuestion(Boolean marqueQuestion) {
        this.marqueQuestion = marqueQuestion;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public Set<Reponse> getListReponse() {
        return listReponse;
    }

    public void setListReponse(Set<Reponse> listReponse) {
        this.listReponse = listReponse;
    }

    public String getLibelleQuestion() {
        return libelleQuestion;
    }

    public void setLibelleQuestion(String libelleQuestion) {
        this.libelleQuestion = libelleQuestion;
    }

    public Set<TestQuestion> getListTest_question() {
        return listTest_question;
    }

    public void setListTest_question(Set<TestQuestion> listTest_question) {
        this.listTest_question = listTest_question;
    }

    public void addReponse(Reponse reponse)
    {
        reponse.setQuestion(this);
        this.listReponse.add(reponse);
    }

    public void removeReponse(Reponse reponse)
    {
        for (Reponse r: this.listReponse) {
            if(r.getIdReponse() == reponse.getIdReponse())
            {
                r.setQuestion(null);
                this.listReponse.remove(r);
            }
        }
    }

    public void AddTest(Test test) {
        TestQuestion testQuestion = new TestQuestion(test, this);
        test.addTestQuestion(testQuestion);
        this.listTest_question.add(testQuestion);
    }

    public void removeTest(Test test)
    {
        for (TestQuestion testQuestion: this.listTest_question) {
            if(testQuestion.getTest().getIdTest() == test.getIdTest())
            {
                testQuestion.getTest().removeTestQuestion(testQuestion);
                this.listTest_question.remove(testQuestion);
            }
        }
    }

    public void addTestQuestion(TestQuestion tq) {
        this.listTest_question.add(tq);
    }

    public void removeTestQuestion(TestQuestion tq)
    {
        this.listTest_question.remove(tq);
    }
}
