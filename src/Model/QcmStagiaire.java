package Model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "qcmStagiaire")
@NamedQueries({
        @NamedQuery( name= "findAllQcmStagiaire", query = "SELECT qs from QcmStagiaire as qs"),
        @NamedQuery( name= "findQcmStagiaireByStagiaire", query = "SELECT qs from QcmStagiaire as qs where qs.stagiaire.id = :id"),
        @NamedQuery( name= "findQcmStagiaireByQcm", query = "SELECT qs from QcmStagiaire as qs where qs.qcm.id = :id")
})
public class QcmStagiaire implements Serializable {

    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fkQcm")
    private QCM qcm;
    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fkStagiaire")
    private Stagiaire stagiaire;

    public QcmStagiaire(){}

    public QcmStagiaire(QCM qcm, Stagiaire stagiaire)
    {
        this.qcm = qcm;
        this.stagiaire = stagiaire;
    }

    public QCM getQcm() {
        return qcm;
    }

    public void setQcm(QCM qcm) {
        this.qcm = qcm;
    }

    public Stagiaire getStagiaire() {
        return stagiaire;
    }

    public void setStagiaire(Stagiaire stagiaire) {
        this.stagiaire = stagiaire;
    }
}
