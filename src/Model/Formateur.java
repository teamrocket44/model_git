package Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "formateur")
@NamedQueries( {
        @NamedQuery( name="loginF", query= " Select formateur FROM Formateur as formateur Where formateur.nomFormateur = :loginF AND formateur.mdpFormateur = :passwordF"),
        @NamedQuery( name= "findAllFormateur", query = "SELECT nomFormateur from Formateur ")
})
public class Formateur implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int idFormateur;
    @Column(unique = true)
    private String emailFormateur;
    private String mdpFormateur;
    private String nomFormateur;
    private String prenomFormateur;

    @OneToMany(mappedBy = "formateur", fetch = FetchType.EAGER)
    private Set<QCM> listQCM;

    @OneToMany(mappedBy = "formateur")
    private Set<Test> listTest;

    public Formateur()
    {
        this.listQCM = new HashSet<>();
        this.listTest = new HashSet<>();
    }

    public int getIdFormateur() {
        return idFormateur;
    }

    public void setIdFormateur(int idFormateur) {
        this.idFormateur = idFormateur;
    }

    public String getEmailFormateur() {
        return emailFormateur;
    }

    public void setEmailFormateur(String emailFormateur) {
        this.emailFormateur = emailFormateur;
    }

    public String getMdpFormateur() {
        return mdpFormateur;
    }

    public void setMdpFormateur(String mdpFormateur) {
        this.mdpFormateur = mdpFormateur;
    }

    public String getNomFormateur() {
        return nomFormateur;
    }

    public void setNomFormateur(String nomFormateur) {
        this.nomFormateur = nomFormateur;
    }

    public String getPrenomFormateur() {
        return prenomFormateur;
    }

    public void setPrenomFormateur(String prenomFormateur) {
        this.prenomFormateur = prenomFormateur;
    }

    public Set<QCM> getListQCM() {
        return listQCM;
    }

    public void setListQCM(Set<QCM> listQCM) {
        this.listQCM = listQCM;
    }

    public Set<Test> getListTest() {
        return listTest;
    }

    public void setListTest(Set<Test> listTest) {
        this.listTest = listTest;
    }


}
