package Model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "qcm")
@NamedQueries( {
        @NamedQuery( name= "findAllQcm", query = "SELECT qcm from QCM as qcm")
})
public class QCM  implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int idQCM;
    private String nomQCM;
    private Time tempsQCM; //temps max

    @ManyToOne
    @JoinColumn(name = "fkFormateur")
    private Formateur formateur;

    @OneToMany(mappedBy = "qcm", fetch = FetchType.EAGER)
    private Set<QcmSection> listQcm_Section;

    @OneToMany(mappedBy = "qcm")
    private Set<Test> listTest;

    @OneToMany(mappedBy = "qcm", fetch = FetchType.EAGER)
    private Set<QcmStagiaire> listQcm_Stagiaire;

    public QCM()
    {
        this.listQcm_Stagiaire = new HashSet<>();
        this.listQcm_Section = new HashSet<>();
        this.listTest = new HashSet<>();
    }

    public int getIdQCM() {
        return idQCM;
    }

    public void setIdQCM(int idQCM) {
        this.idQCM = idQCM;
    }

    public String getNomQCM() {
        return nomQCM;
    }

    public void setNomQCM(String nomQCM) {
        this.nomQCM = nomQCM;
    }

    public Time getTempsQCM() {
        return tempsQCM;
    }

    public void setTempsQCM(Time tempsQCM) {
        this.tempsQCM = tempsQCM;
    }

    public Formateur getFormateur() {
        return formateur;
    }

    public void setFormateur(Formateur formateur) {
        this.formateur = formateur;
    }

    public Set<QcmSection> getListQcm_Section() {
        return listQcm_Section;
    }

    public void setListQcm_Section(Set<QcmSection> listQcm_Section) {
        this.listQcm_Section = listQcm_Section;
    }

    public Set<QcmStagiaire> getListQcm_Stagiaire() {
        return listQcm_Stagiaire;
    }

    public void setListQcm_Stagiaire(Set<QcmStagiaire> listQcm_Stagiaire) {
        this.listQcm_Stagiaire = listQcm_Stagiaire;
    }

    public Set<Test> getListTest() {
        return listTest;
    }

    public void setListTest(Set<Test> listTest) {
        this.listTest = listTest;
    }

    //Stagiaire

    public void addStagiaire(Stagiaire stagiaire)
    {
        QcmStagiaire qs = new QcmStagiaire(this, stagiaire);
        stagiaire.addQcmStagiaire(qs);
        this.listQcm_Stagiaire.add(qs);
    }

    public void removeStagiaire(Stagiaire stagiaire)
    {
        for (QcmStagiaire qs: this.listQcm_Stagiaire) {
            if(qs.getStagiaire().getIdStagiaire() == stagiaire.getIdStagiaire())
            {
                qs.getStagiaire().removeQcmStagiaire(qs);
                this.listQcm_Stagiaire.remove(qs);
            }
        }
    }

    public void addQcmStagiaire(QcmStagiaire qs) {
        this.listQcm_Stagiaire.add(qs);
    }

    public void removeQcmStagiaire(QcmStagiaire qs)
    {
        this.listQcm_Stagiaire.remove(qs);
    }

    // Section

    public void addSection(Section section)
    {
        QcmSection qs = new QcmSection(this, section);
        section.addQcmSection(qs);
        this.listQcm_Section.add(qs);
    }

    public void removeSection(Section section)
    {
        for (QcmSection qs: this.listQcm_Section) {
            if(qs.getSection().getIdSection() == section.getIdSection())
            {
                qs.getSection().removeQcmSection(qs);
                this.listQcm_Section.remove(qs);
            }
        }
    }

    public void addQcmSection(QcmSection qs) {
        this.listQcm_Section.add(qs);
    }

    public void removeQcmSection(QcmSection qs)
    {
        this.listQcm_Section.remove(qs);
    }
}
