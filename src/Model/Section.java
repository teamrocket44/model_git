package Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "section")
@NamedQueries( {
        @NamedQuery( name= "findAllSection", query = "SELECT section from Section as section")
})
public class Section  implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int idSection;
    private String nomSection;

    @OneToMany(mappedBy = "section")
    private Set<QcmSection> listQcm_Section;

    @OneToMany(mappedBy = "section", cascade = CascadeType.ALL)
    private Set<Question> listQuestion;

    public Section()
    {
        this.listQuestion = new HashSet<>();
        this.listQcm_Section = new HashSet<>();
    }

    public int getIdSection() {
        return idSection;
    }

    public void setIdSection(int idSection) {
        this.idSection = idSection;
    }

    public String getNomSection() {
        return nomSection;
    }

    public void setNomSection(String nomSection) {
        this.nomSection = nomSection;
    }

    //QCM

    public Set<QcmSection> getListQcm_Section() {
        return listQcm_Section;
    }

    public void setListQcm_Section(Set<QcmSection> listQcm_Section) {
        this.listQcm_Section = listQcm_Section;
    }

    public void AddQCM(QCM qcm) {
        QcmSection qs = new QcmSection(qcm, this);
        qcm.addQcmSection(qs);
        this.listQcm_Section.add(qs);
    }

    public void removeQCM(QCM qcm)
    {
        for (QcmSection qs: this.listQcm_Section) {
            if(qs.getQcm().getIdQCM() == qcm.getIdQCM())
            {
                qs.getQcm().removeQcmSection(qs);
                this.listQcm_Section.remove(qs);
            }
        }
    }

    //Question

    public Set<Question> getListQuestion() {
        return listQuestion;
    }

    public void setListQuestion(Set<Question> listQuestion) {
        this.listQuestion = listQuestion;
    }

    public void addQuestion(Question question)
    {
        question.setSection(this);
        this.listQuestion.add(question);
    }

    public void removeQuestion(Question question)
    {
        for (Question q: this.listQuestion) {
            if(q.getIdQuestion() == question.getIdQuestion())
            {
                q.setSection(null);
                this.listQuestion.remove(q);
            }
        }
    }

    public void addQcmSection(QcmSection qcm) {
        this.listQcm_Section.add(qcm);
    }

    public void removeQcmSection(QcmSection qs)
    {
        this.listQcm_Section.remove(qs);
    }
}
