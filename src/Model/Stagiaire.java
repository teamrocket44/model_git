package Model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="stagiaire")
@NamedQueries( {
        @NamedQuery( name="login", query= " Select stagiaire FROM Stagiaire as stagiaire Where stagiaire.nomStagaire = :login AND stagiaire.mdpStagiaire = :password"),
        @NamedQuery( name= "findAllUsers", query = "SELECT nomStagaire from Stagiaire")
})

public class Stagiaire implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int idStagiaire;
    @Column(name="nomStagiaire")
    private String nomStagaire;

    @Column(name="prenomStagiaire")
    private String prenomStagiaire;

    @Column(name="mdpStagiaire")
    private String mdpStagiaire;

    @Column(name="emailStagiaire")
    private String emailStagaire;

    @OneToMany(mappedBy = "stagiaire",fetch = FetchType.EAGER)
    private Set<QcmStagiaire> listQcm_Stagiaire;

    @OneToMany(mappedBy = "stagiaire",fetch = FetchType.EAGER)
    private Set<Test> listTest;

    public Stagiaire()
    {
        this.listTest = new HashSet<>();
        this.listQcm_Stagiaire = new HashSet<>();
    }

    public String getNomStagaire() {
        return nomStagaire;
    }

    public void setNomStagaire(String nomStagaire) {
        this.nomStagaire = nomStagaire;
    }

    public String getPrenomStagiaire() {
        return prenomStagiaire;
    }

    public void setPrenomStagiaire(String prenomStagiaire) {
        this.prenomStagiaire = prenomStagiaire;
    }

    public String getMdpStagiaire() {
        return mdpStagiaire;
    }

    public void setMdpStagiaire(String mdpStagiaire) {
        this.mdpStagiaire = mdpStagiaire;
    }

    public String getEmailStagaire() {
        return emailStagaire;
    }

    public void setEmailStagaire(String emailStagaire) {
        this.emailStagaire = emailStagaire;
    }

    public int getIdStagiaire() {
        return idStagiaire;
    }

    public void setIdStagiaire(int idStagiaire) {
        this.idStagiaire = idStagiaire;
    }

    // Test

    public Set<Test> getListTest() {
        return listTest;
    }

    public void setListTest(Set<Test> listTest) {
        this.listTest = listTest;
    }

    public void addTest(Test test)
    {
        test.setStagiaire(this);
        this.listTest.add(test);
    }

    public void removeTest(Test test)
    {
        for (Test t: this.listTest) {
            if(t.getIdTest() == test.getIdTest())
            {
                t.setStagiaire(null);
                this.listTest.remove(t);
            }
        }
    }

    //QCM

    public void addQcmStagiaire(QcmStagiaire qs) {
        this.listQcm_Stagiaire.add(qs);
    }

    public void removeQcmStagiaire(QcmStagiaire qs)
    {
        this.listQcm_Stagiaire.remove(qs);
    }

    public void AddQCM(QCM qcm) {
        QcmStagiaire qs = new QcmStagiaire(qcm, this);
        qcm.addQcmStagiaire(qs);
        this.listQcm_Stagiaire.add(qs);
    }

    public void removeQCM(QCM qcm)
    {
        for (QcmStagiaire qs: this.listQcm_Stagiaire) {
            if(qs.getQcm().getIdQCM() == qcm.getIdQCM())
            {
                qs.getQcm().removeQcmStagiaire(qs);
                this.listQcm_Stagiaire.remove(qs);
            }
        }
    }

}
