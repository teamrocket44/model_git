package Model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "qcmSection")
@NamedQueries({
        @NamedQuery( name= "findAllQcmSection", query = "SELECT qs from QcmSection as qs"),
        @NamedQuery( name= "findQcmSectionBySection", query = "SELECT qs from QcmSection as qs where qs.section.id = :id"),
        @NamedQuery( name= "findQcmSectionByQcm", query = "SELECT qs from QcmSection as qs where qs.qcm.id = :id")
})
public class QcmSection implements Serializable {

    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fkQcm")
    private QCM qcm;
    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fkSection")
    private Section section;
    private int nbquestionbySection;

    public QcmSection(){}

    public QcmSection(QCM qcm, Section sectionn)
    {
        this.qcm = qcm;
        this.section = section;
    }

    public QcmSection(QCM qcm, Section sectionn, int nbquestionbySection)
    {
        this.qcm = qcm;
        this.section = section;
        this.nbquestionbySection = nbquestionbySection;
    }

    public QCM getQcm() {
        return qcm;
    }
    public void setQcm(QCM qcm) {
        this.qcm = qcm;
    }
    public Section getSection() {
        return section;
    }
    public void setSection(Section section) {
        this.section = section;
    }

    public int getNbquestionbySection() {
        return nbquestionbySection;
    }

    public void setNbquestionbySection(int nbquestionbySection) {
        this.nbquestionbySection = nbquestionbySection;
    }
}
