package Model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name = "testQuestion")
public class TestQuestion implements Serializable {

    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fkQuestion")
    private Question question;
    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fkTest")
    private Test test;

    private Date date;


    private Boolean rep_OK;

    public TestQuestion(){}

    public TestQuestion(Test test, Question question) {
        this.test = test;
        this.question = question;
        this.date = new Date(System.currentTimeMillis());
    }

    public TestQuestion(Test test, Question question, Date date) {
        this.test = test;
        this.question = question;
        this.date = date;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
