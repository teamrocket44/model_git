package Model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "reponse")
@NamedQueries( {
        @NamedQuery( name= "findAllReponse", query = "SELECT reponse from Reponse as reponse")
})
public class Reponse  implements Serializable {

    public enum Numero
    {
        A ("A"), B ("B"), C ("C"), D ("D");
        private String label;
        Numero( String l)
        {
            this.label = l;
        }
        @Override
        public String toString()
        {
            return label;
        }
    }
    @Transient
    private Numero numero;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int idReponse;
    private String libelleReponse;
    private Boolean reponse_o_n;

    @ManyToOne
    @JoinColumn(name = "fkQuestion")
    private Question question;

    public Reponse(String libelleReponse, boolean reponse_o_n)
    {
        this.libelleReponse=libelleReponse;
        this.reponse_o_n=reponse_o_n;

    }
    public  Reponse ()
    {

    }

    public int getIdReponse() {
        return idReponse;
    }

    public void setIdReponse(int idReponse) {
        this.idReponse = idReponse;
    }

    public String getLibelleReponse() {
        return libelleReponse;
    }

    public void setLibelleReponse(String libelleReponse) {
        this.libelleReponse = libelleReponse;
    }

    public Boolean getReponse_o_n() {
        return reponse_o_n;
    }

    public void setReponse_o_n(Boolean reponse_o_n) {
        this.reponse_o_n = reponse_o_n;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

}
