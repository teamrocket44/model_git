package Model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "test")
@NamedQueries( {
        @NamedQuery( name= "findAllTest", query = "SELECT test from Test as test")
})
public class Test  implements Serializable {

    public Test(String nomTest,int tempsTest) {
        this.nomTest = nomTest;
        this.tempsTest=tempsTest;
        this.listQuestion_Test = new HashSet<>();
    }
    public Test() {
        this.listQuestion_Test = new HashSet<>();
    }

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int idTest;
    private String nomTest;
    private int tempsTest; //temps restant
    private Date dateTest;

    @ManyToOne
    @JoinColumn(name = "fkFormateur")
    private Formateur formateur;

    @ManyToOne
    @JoinColumn(name = "fkQcm")
    private QCM qcm;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fkStagiaire")
    private Stagiaire stagiaire;

    @OneToMany(mappedBy = "test", fetch = FetchType.EAGER)
    private Set<TestQuestion> listQuestion_Test;

    public int getIdTest() {
        return idTest;
    }

    public void setIdTest(int idTest) {
        this.idTest = idTest;
    }

    public String getNomTest() {
        return nomTest;
    }

    public void setNomTest(String nomTest) {
        this.nomTest = nomTest;
    }

    public int getTempsTest() {
        return tempsTest;
    }

    public void setTempsTest(int tempsTest) {
        this.tempsTest = tempsTest;
    }

    public Date getDateTest() {
        return dateTest;
    }

    public void setDateTest(Date dateTest) {
        this.dateTest = dateTest;
    }

    public Formateur getFormateur() {
        return formateur;
    }

    public void setFormateur(Formateur formateur) {
        this.formateur = formateur;
    }

    public QCM getQcm() {
        return qcm;
    }

    public void setQcm(QCM qcm) {
        this.qcm = qcm;
    }

    public Stagiaire getStagiaire() {
        return stagiaire;
    }

    public void setStagiaire(Stagiaire stagiaire) {
        this.stagiaire = stagiaire;
    }

    public Set<TestQuestion> getListQuestion_Test() {
        return listQuestion_Test;
    }

    public void setListQuestion_Test(Set<TestQuestion> listQuestion_Test) {
        this.listQuestion_Test = listQuestion_Test;
    }

    public void AddQuestion(Question question) {
        TestQuestion testQuestion = new TestQuestion(this, question);
        question.addTestQuestion(testQuestion);
        this.listQuestion_Test.add(testQuestion);
    }

    public void removeQuestion(Question question)
    {
        for (TestQuestion testQuestion: this.listQuestion_Test) {
            if(testQuestion.getQuestion().getIdQuestion() == question.getIdQuestion())
            {
                testQuestion.getQuestion().removeTestQuestion(testQuestion);
                this.listQuestion_Test.remove(testQuestion);
            }
        }
    }

    public void addTestQuestion(TestQuestion tq) {
        this.listQuestion_Test.add(tq);
    }

    public void removeTestQuestion(TestQuestion tq)
    {
        this.listQuestion_Test.remove(tq);
    }
}
